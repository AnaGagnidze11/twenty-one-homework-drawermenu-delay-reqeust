package com.example.navdrawerdelayrequest.network

import com.example.navdrawerdelayrequest.models.AdviceModel
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepository {
    @GET("/advice")
    suspend fun getAdvices(): Response<AdviceModel>
}