package com.example.navdrawerdelayrequest.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.navdrawerdelayrequest.models.AdviceModel
import com.example.navdrawerdelayrequest.network.ResultControl
import com.example.navdrawerdelayrequest.network.RetrofitService
import kotlinx.coroutines.*

class HomeViewModel : ViewModel() {

    private val adviceLiveData = MutableLiveData<ResultControl<AdviceModel>>().apply {
        mutableListOf<AdviceModel>()
    }
    val _adviceLiveData: LiveData<ResultControl<AdviceModel>> = adviceLiveData


    fun initArticle(): Job{
        return viewModelScope.launch {
            withContext(Dispatchers.IO){
                while (isActive)
                    getAdvice()
                    delay(10000)
            }
        }
    }

    private suspend fun getAdvice(){
        adviceLiveData.postValue(ResultControl.loading(true))
        val result = RetrofitService.retrofitService.getAdvices()
        if (result.isSuccessful){
            val article = result.body()
            article?.let {
                adviceLiveData.postValue(ResultControl.success(it))
            }

        }else{
            adviceLiveData.postValue(ResultControl.error(result.message()))
        }
    }
}