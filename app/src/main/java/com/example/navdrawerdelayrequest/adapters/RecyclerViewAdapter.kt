package com.example.navdrawerdelayrequest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.navdrawerdelayrequest.models.DrawableItem
import com.example.navdrawerdelayrequest.R
import com.example.navdrawerdelayrequest.databinding.ItemLayoutBinding

typealias OnDrawableClick = (position: Int) -> Unit

class RecyclerViewAdapter(private val items: MutableList<DrawableItem>) :
    RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

    private var currentDrawablePosition: Int = 0
    lateinit var onDrawableClick: OnDrawableClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = items.size

    inner class ItemViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var item: DrawableItem

        fun bind() {
            item = items[adapterPosition]
            binding.imageOfDrawable.setImageResource(item.icon)
            binding.TitleTxtDrawable.text = item.title
            binding.root.setOnClickListener(this)

            if (currentDrawablePosition == adapterPosition) {
                binding.itemBackground.setBackgroundColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.chosen_background_color
                    )
                )
                binding.chosenDrawableView.visibility = View.VISIBLE
            }else{
                binding.itemBackground.setBackgroundColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.white
                    )
                )

                binding.chosenDrawableView.visibility = View.INVISIBLE
            }
        }

        override fun onClick(v: View?) {
            currentDrawablePosition = adapterPosition
            notifyDataSetChanged()
            onDrawableClick.invoke(adapterPosition)
        }
    }
}