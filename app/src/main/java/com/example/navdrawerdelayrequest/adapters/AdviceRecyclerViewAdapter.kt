package com.example.navdrawerdelayrequest.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.navdrawerdelayrequest.databinding.AdviceItemLayoutBinding
import com.example.navdrawerdelayrequest.models.AdviceModel

class AdviceRecyclerViewAdapter: RecyclerView.Adapter<AdviceRecyclerViewAdapter.AdviceViewHolder>() {

    private val advices = mutableListOf<AdviceModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdviceViewHolder {
        return AdviceViewHolder(AdviceItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: AdviceViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = advices.size

    inner class AdviceViewHolder(private val binding: AdviceItemLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var advice: AdviceModel
        fun bind(){
            advice = advices[adapterPosition]
            binding.adviceTxt.text = advice.slip?.advice
        }
    }


    fun setData(advice: AdviceModel){
        advices.add(advice)
        notifyItemInserted(advices.size)
    }
}
