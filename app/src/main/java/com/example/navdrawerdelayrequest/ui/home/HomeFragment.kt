package com.example.navdrawerdelayrequest.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.navdrawerdelayrequest.adapters.AdviceRecyclerViewAdapter
import com.example.navdrawerdelayrequest.databinding.FragmentHomeBinding
import com.example.navdrawerdelayrequest.models.AdviceModel
import com.example.navdrawerdelayrequest.network.ResultControl
import com.example.navdrawerdelayrequest.viewmodels.HomeViewModel
import kotlinx.coroutines.Job

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: AdviceRecyclerViewAdapter

    private val homeViewModel: HomeViewModel by viewModels()
    private lateinit var getManyAdvices: Job

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        if (_binding == null){
            _binding = FragmentHomeBinding.inflate(inflater, container, false)

        }
        init()
        return binding.root
    }

    private fun init(){

        getManyAdvices = homeViewModel.initArticle()
        observe()

        adapter = AdviceRecyclerViewAdapter()
        binding.adviceRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.adviceRecyclerView.adapter = adapter


    }

    private fun observe(){
        homeViewModel._adviceLiveData.observe(viewLifecycleOwner, {
            when (it.status) {
                ResultControl.Status.SUCCESS -> {
                    it.data?.let { it1 -> adapter.setData(it1) }
                }

                ResultControl.Status.ERROR -> {
                    Toast.makeText(requireContext(), "${it.message}", Toast.LENGTH_SHORT).show()

                }

                ResultControl.Status.LOADING -> { }
            }
        })
    }


    override fun onStop() {
        super.onStop()
        getManyAdvices.cancel()

    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}