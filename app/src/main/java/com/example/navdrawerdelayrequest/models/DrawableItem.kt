package com.example.navdrawerdelayrequest.models

data class DrawableItem(val icon: Int, val title: String)
