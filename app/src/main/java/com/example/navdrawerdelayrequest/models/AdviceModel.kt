package com.example.navdrawerdelayrequest.models

data class AdviceModel(
    val slip: Slip?
)

data class Slip(
    val advice: String?,
    val id: Int?
)